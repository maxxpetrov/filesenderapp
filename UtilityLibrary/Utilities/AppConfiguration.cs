﻿using System;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace UtilityLibrary.Utilities
{
    public class AppConfiguration : IAppConfiguration
    {
        public IConfiguration Config(string configFileName)
        {
            var config = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                                   .AddJsonFile(configFileName);
            return config.Build();
        }
    }
}
