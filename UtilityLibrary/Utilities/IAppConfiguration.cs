﻿using Microsoft.Extensions.Configuration;

namespace UtilityLibrary.Utilities
{
    public interface IAppConfiguration
    {
        IConfiguration Config(string configFileName);
    }
}