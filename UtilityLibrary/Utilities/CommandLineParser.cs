﻿using System;
using System.Collections.Generic;
using System.Text;
using CommandLine;

namespace UtilityLibrary.Utilities
{
    public class CommandLineParser : ICommandLineParser
    {
        public Options Parse(string[] args)
        {
            var parser = Parser.Default;
            return parser.ParseArguments<Options>(args).MapResult((Options o) => o, errs => null);
        }
    }    
}
