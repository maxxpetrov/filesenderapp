﻿namespace UtilityLibrary.Utilities
{
    public interface ICommandLineParser
    {
        Options Parse(string[] args);
    }
}