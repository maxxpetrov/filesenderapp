﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityLibrary
{
    public class Options
    {

        [Option('p', "path", Required = true, HelpText = "Path to file on local PC")]
        public string LocalFilePath { get; set; }

        [Option('n', "name", Required = false, HelpText = "File name on SFTP server")]
        public string RemoteFileName { get; set; }

        [Option('c', "config", Default = "appsettings.json", HelpText = "Optional config file name")]
        public string ConfigName { get; set; }

    }
}
