﻿using Autofac.Features.Indexed;
using IFileSenderUtil;
using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityLibrary;
using UtilityLibrary.Utilities;

namespace FileSenderApp
{
    public class Application : IApplication
    {
        ICommandLineParser parser;
        IAppConfiguration config;
        IIndex<string, IFileSender> _senders;
        IFileSender fileSender;
        private readonly ILogger _logger;

        public Application(ICommandLineParser cmdParser, IAppConfiguration appConfig
                           , IIndex<string, IFileSender> senders, ILogger logger)
        {
            parser = cmdParser;
            config = appConfig;
            _senders = senders;
            _logger = logger;
        }
        public int Run(string[] args)
        {
            int retCode = 0;
            Options cmdArgs = parser.Parse(args);
            if (cmdArgs == null) retCode = -1;
            IConfiguration conf = config.Config(cmdArgs.ConfigName);
            foreach (var sender in conf["sendto"].Split(';'))
            {
                try
                {
                    fileSender = _senders[sender];
                    if (fileSender.UploadFiles(args) != 0)
                    {
                        _logger.Info("wrong sendto settings"); //потом переместить в модуль котрый посылает почту, в лог надо выводить сеттинги которые вызвали проблемы
                        retCode = -1;
                    };
                }
                catch (Exception e)
                {
                    _logger.Error(e,"Error");
                    retCode = -1;
                }
            }
            return retCode;
        }
    }
}
