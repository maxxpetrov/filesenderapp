﻿using Autofac;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UtilityLibrary;
using System.Text;
using Autofac.Extras.NLog;

namespace FileSenderApp
{
    class ContainerConfig
    {
        public static IContainer Configure()
        {
            List<Assembly> allAssemblies = new List<Assembly>();
            foreach (string dll in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*Util.dll", SearchOption.AllDirectories))
                allAssemblies.Add(Assembly.UnsafeLoadFrom(dll));
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>().As<IApplication>();
            builder.RegisterAssemblyTypes(Assembly.Load(nameof(UtilityLibrary)))
                .Where(t => t.Namespace.Contains("Utilities"))
                .As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "I" + t.Name))
                .SingleInstance();
            builder.RegisterAssemblyModules(allAssemblies.ToArray());
            builder.RegisterModule<NLogModule>();

            return builder.Build();
        }
    }
}
