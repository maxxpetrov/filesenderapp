﻿using Autofac;
using System;

namespace FileSenderApp
{
    class Program
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        static int Main(string[] args)
        {
            LogUnhandledExceptions();

            Logger.Info($"App is running with args {string.Join(",", args)}");

            try
            {
                var container = ContainerConfig.Configure();

                using (var scope = container.BeginLifetimeScope())
                {
                    var app = scope.Resolve<IApplication>();
                    return app.Run(args);
                }
            }
            catch (Exception e)
            {

                Logger.Error(e);
                return -1;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        static void LogUnhandledExceptions()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, exception) =>
            {
                if (exception.ExceptionObject is Exception)
                    Logger.Fatal((Exception)exception.ExceptionObject, "Fatal");
                else
                    Logger.Fatal($"Fatal {exception.ExceptionObject.ToString()}");
            };
        }
    }
}
