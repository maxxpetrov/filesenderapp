﻿namespace FileSenderApp
{
    public interface IApplication
    {
        int Run(string[] args);
    }
}