﻿using Autofac;
using IFileSenderUtil;

namespace EmailFileSenderUtil
{
    class EmailModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmailFileSender>().Keyed<IFileSender>("email");
        }
    }
}