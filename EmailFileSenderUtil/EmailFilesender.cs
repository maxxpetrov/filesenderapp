﻿using IFileSenderUtil;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System.IO;
using UtilityLibrary;
using UtilityLibrary.Utilities;

namespace EmailFileSenderUtil
{
    public class EmailFileSender : IFileSender
    {
        ICommandLineParser parser;
        IAppConfiguration config;
        public EmailFileSender(ICommandLineParser cmdParser, IAppConfiguration appConfig)
        {
            parser = cmdParser;
            config = appConfig;
        }
        public int UploadFiles(string[] args)
        {
            Options cmdOptions = parser.Parse(args);
            if (cmdOptions == null) return -1;
            IConfiguration conf = config.Config(cmdOptions.ConfigName);
            if (!GetFileNames(cmdOptions, out var localFile))
            {
                return -1;
            }

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(conf["senders:email:username"]));
            foreach (var item in conf["senders:email:sendto"].Split(','))
            {
                message.To.Add(new MailboxAddress(item));
            }
            message.Subject = localFile.Name;
            var builder = new BodyBuilder();
            builder.Attachments.Add(localFile.FullName);
            message.Body = builder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.Connect(conf["senders:email:host"], int.Parse(conf["senders:email:port"]), bool.Parse(conf["senders:email:EnableSsl"]));
                client.Authenticate(conf["senders:email:username"], conf["senders:email:password"]);
                client.Send(message);
                client.Disconnect(true);
            }
            return 0;
        }

        static bool GetFileNames(Options options, out FileInfo localFile)
        {
            localFile = new FileInfo(options.LocalFilePath);         

            if (!localFile.Exists)
            {
                //Logger.Info($"The file does not exist {options.LocalFilePath}");
                return false;
            }                       

            return true;

        }
    }
}
