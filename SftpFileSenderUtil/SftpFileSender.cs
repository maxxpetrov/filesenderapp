﻿using IFileSenderUtil;
using Microsoft.Extensions.Configuration;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UtilityLibrary;
using UtilityLibrary.Utilities;

namespace SftpFileSenderUtil
{
    public class SftpFileSender : IFileSender
    {
        ICommandLineParser parser;
        IAppConfiguration config;
        public SftpFileSender(ICommandLineParser cmdParser, IAppConfiguration appConfig)
        {
            parser = cmdParser;
            config = appConfig;
        }
        public int UploadFiles(string[] args)
        {
            Options cmdOptions = parser.Parse(args);
            if (cmdOptions == null) return -1;
            IConfiguration conf = config.Config(cmdOptions.ConfigName);
            if (!GetFileNames(cmdOptions, out var localFile, out var remoteFileName))
            {
                return -1;
            }
            using (SftpClient sftp = new SftpClient(conf["senders:sftp:host"]
                                                   , int.Parse(conf["senders:sftp:port"])
                                                   , conf["senders:sftp:username"]
                                                   , conf["senders:sftp:password"]))
            {

                sftp.Connect();

                var fileStream = new FileStream(localFile.FullName, FileMode.Open);
                if (fileStream != null)
                {
                    sftp.UploadFile(fileStream, conf["senders:sftp:remoteDirectory"] + "/" + remoteFileName, null);                    
                }
                sftp.Disconnect();
                return 0;
            }
        }

        static bool GetFileNames(Options options, out FileInfo localFile, out string remoteFileName)
        {
            localFile = new FileInfo(options.LocalFilePath);
            remoteFileName = null;

            if (!localFile.Exists)
            {
                // Logger.Info($"The file does not exist {options.LocalFilePath}");
                return false;
            }

            remoteFileName = !string.IsNullOrEmpty(options.RemoteFileName) ? options.RemoteFileName : localFile.Name;

            return true;

        }
    }
}