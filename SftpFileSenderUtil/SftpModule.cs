﻿using Autofac;
using IFileSenderUtil;

namespace SftpFileSenderUtil
{
    class SftpModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SftpFileSender>().Keyed<IFileSender>("sftp");
        }

    }
}