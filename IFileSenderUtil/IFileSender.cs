﻿namespace IFileSenderUtil
{
    public interface IFileSender
    {
        int UploadFiles(string[] args);
    }
}
