﻿using Renci.SshNet.Common;
using SftpFileSenderUtil;
using System;
using UtilityLibrary.Utilities;
using Xunit;

namespace FileSenderApp.Tests
{
    public class SftpSenderTests
    {
        [Fact]
        public void UploadFiles_InvalidSFTPAddressShouldFail()
        {
            //Arrange
            ICommandLineParser parser = new CommandLineParser();
            IAppConfiguration appConfig = new AppConfiguration();
            SftpFileSender sender = new SftpFileSender(parser, appConfig);
            string[] args = { "-p", @"C:\SSUUpdater.log", "-c", @"C:\Users\Maxx\Downloads\appsettings.json" };

            //Act
            //Assert
            Assert.Throws<AggregateException>(() => sender.UploadFiles(args));
        }
        [Fact]
        public void UploadFiles_InvalidUserNameShouldFail()
        {
            //Arrange
            ICommandLineParser parser = new CommandLineParser();
            IAppConfiguration appConfig = new AppConfiguration();
            SftpFileSender sender = new SftpFileSender(parser, appConfig);
            string[] args =  { "-p", @"C:\SSUUpdater.log", "-c", @"C:\Users\Maxx\Downloads\appsettings.json" };

            //Act
            //Assert
            Assert.Throws<SshAuthenticationException>(() => sender.UploadFiles(args));
        }
    }
}
